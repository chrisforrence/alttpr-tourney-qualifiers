<?php

namespace App\Http\Controllers;

use App\Models\ConnectedAccount;
use App\Models\User;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class OauthController extends Controller
{
    public function redirectToProvider(Request $request, string $provider)
    {
        session()->put('previous_url', back()->getTargetUrl());
        return Socialite::driver($provider)->redirect();
    }

    public function handleCallback(Request $request, string $provider)
    {
        $providerUser = Socialite::driver($provider)->stateless()->user();

        // Check for a matching profile
        $account = ConnectedAccount::firstOrNew([
            'provider' => $provider,
            'provider_id' => $providerUser->getId(),
        ], [
            'name' => $providerUser->getName(),
            'nickname' => $providerUser->getNickname(),
            'email' => $providerUser->getEmail(),
            'avatar_path' => $providerUser->getAvatar(),
            'token' => $providerUser->token,
            'refresh_token' => $providerUser->refreshToken,
            'expires_at' => now()->addSeconds($providerUser->expiresIn),
        ]);

        $user = User::firstOrCreate([
            'email' => $providerUser->getEmail(),
        ], [
            'name' => $providerUser->getNickname(),
        ]);

        $user->connectedAccounts()->save($account);

        auth()->login($user);

        return redirect(session()->get('previous_url', '/'));
    }
}
