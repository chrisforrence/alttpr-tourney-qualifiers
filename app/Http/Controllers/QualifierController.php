<?php

namespace App\Http\Controllers;

use App\Models\Entrant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class QualifierController extends Controller
{
    public function estimated()
    {
        $entrants = Cache::remember('entrants', 86400, function () {
            return collect(Entrant::all())->sortByDesc('score')->values();
        });
        return response()->view('qualifiers.estimated', ['entrants' => $entrants]);
    }

    public function actual()
    {
        $entrants = Cache::remember('entrants-without-estimates', 86400, function () {
            return collect(Entrant::all())->sortByDesc('current_score')->values();
        });
        return response()->view('qualifiers.actual', ['entrants' => $entrants]);
    }
}
