<?php

namespace App\Console\Commands;

use App\Models\Entrant;
use App\Models\Race;
use App\Models\RaceResult;
use Carbon\Carbon;
use Illuminate\Console\Command;

class AdjustResults extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'adjust:results {race} {racer} {time}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Overwrites the finish time for a race/racer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $entrant = Entrant::where('name', $this->argument('racer'))->firstOrFail();
        $race = Race::where('slug', $this->argument('race'))->firstOrFail();
        $result = RaceResult::where('entrant_id', $entrant->id)->where('race_id', $race->id)->firstOrFail();
        $includes_study = true;

        if (config('app.study_time_seconds') > 0) {
            $includes_study = $this->confirm('Does the entered time take the study time into account? (Selecting no will subtract ' . config('app.study_time_seconds') . ' seconds)');
        }

        // If this is a disqualification, the racer's time is changed to match that of the Did Not Finish times.
        if (in_array(strtolower($this->argument('time')), ['dnf', 'dq', 'ff'])) {
            $finish_seconds = (15 * 60) + RaceResult::where('race_id', $race->id)->whereNotNull('finish_seconds')->max('finish_seconds');
        } else {
            $ts = Carbon::createFromTimeString($this->argument('time'), 'UTC');
            $finish_seconds = ($ts->hour * 60 * 60) + ($ts->minute * 60) + ($ts->second) - ($includes_study ? 0 : config('app.study_time_seconds'));
        }

        $result->original_finish_seconds = $result->finish_seconds;
        $result->finish_seconds = $finish_seconds;
        $result->save();

        $race->recalculatePar();

        // Cache things
        $this->line('Caching...');
        Cache::remember('races', 86400, function () {
            return Race::orderBy('started_at')->get();
        });
        Cache::remember('entrants', 86400, function () {
            return collect(Entrant::all())->sortByDesc('score')->values();
        });

        $this->info(
            'Updated! ' . $entrant->name . ' adjusted in race ' . $race->slug
            . ' from ' . gmdate('H:i:s', $result->original_finish_seconds)
            . ' to ' . gmdate('H:i:s', $result->finish_seconds)
        );
        return 0;
    }
}
