<?php

namespace App\Console\Commands;

use App\Jobs\WatchRace;
use Carbon\Carbon;
use DateInterval;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Console\Command;
use App\Models\Race;
use App\Models\Entrant;
use App\Models\RaceResult;
use Illuminate\Support\Str;

class ProcessResults extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:results {slug}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Processes the results from the given RT.gg room';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $slug = $this->argument('slug');
        Race::where('slug', $slug)->delete();

        $results = (Http::get('https://racetime.gg/alttpr/' . urlencode($slug) . '/data'))->json();
        $info = $results['info'];
        $opened_at = Carbon::parse($results['opened_at']);
        $started_at = Carbon::parse($results['started_at']);
        $ended_at = $results['ended_at'] === null ? null : Carbon::parse($results['ended_at']);

        $seed = null;
        if (preg_match('/https:\/\/alttpr\.com\/h\/([^\s]*)/', $info, $matches)) {
            $seed = $matches[0];
        }

        $race = Race::create([
            'slug' => $slug,
            'seed' => $seed,
            'opened_at' => $opened_at,
            'started_at' => $started_at,
            'ended_at' => $ended_at,
        ]);

        foreach ($results['entrants'] as $entrant) {
            $name = $entrant['user']['full_name'];
            $pronouns = $entrant['user']['pronouns'];
            $status = $entrant['status']['value'];

            // This runner is still on the field; do not show their results
            if (in_array($status, ['not_ready', 'ready', 'in_progress'])) {
                continue;
            }

            $time = explode('.', $entrant['finish_time'])[0] . 'S';

            $entrant = Entrant::where('name', $name)->firstOrCreate(
                [ 'name' => $name ],
                [ 'pronouns' => $pronouns, 'slug' => Str::slug($name) ]
            );

            $finish_seconds = null;
            if ($status === 'done') {
                $ts = new DateInterval($time);
                $finish_seconds = ($ts->h * 3600) + ($ts->i * 60) + ($ts->s) - config('app.study_time_seconds');
            }

            RaceResult::create([
                'entrant_id' => $entrant->id,
                'race_id' => $race->id,
                'finish_seconds' => $finish_seconds,
                'is_finished' => $finish_seconds !== null,
            ]);
        }

        // Now we have all the data, let's calculate the DNF time!
        RaceResult::where('race_id', $race->id)->whereNull('finish_seconds')->update([
            'finish_seconds' => (15 * 60) + RaceResult::where('race_id', $race->id)->whereNotNull('finish_seconds')->max('finish_seconds')
        ]);

        $race->recalculatePar();

        // Now that we have our race information saved, clear the cache
        cache()->flush();

        if ($results['ended_at'] === null && $results['cancelled_at'] === null) {
            WatchRace::dispatch($slug)->delay(now()->addMinutes(10));
        }

        // Cache things
        $this->line('Caching...');
        Cache::remember('races', 86400, function () {
            return Race::orderBy('started_at')->get();
        });
        Cache::remember('entrants', 86400, function () {
            return collect(Entrant::all())->sortByDesc('score')->values();
        });

        $this->info('Succeeded! Par was ' . gmdate('H:i:s', $race->par_seconds ?? 0));

        return 0;
    }
}
