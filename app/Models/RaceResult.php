<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RaceResult extends Model
{
    use HasFactory;
    public $timestamps = false;
    public $guarded = [];
    protected $appends = ['score', 'time'];

    public function entrant()
    {
        return $this->belongsTo(Entrant::class);
    }

    public function race()
    {
        return $this->belongsTo(Race::class);
    }

    public function getTimeAttribute()
    {
        return gmdate('H:i:s', $this->finish_seconds);
    }

    public function getScoreAttribute()
    {
        return round(min(105, max(0, 100 * (2 - ($this->finish_seconds / $this->race->par_seconds)))), 2);
    }
}
