<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Entrant extends Model
{
    use HasFactory;

    public $guarded = [];
    protected $appends = [
        'score',
        'current_score',
        'estimated_score',
        'rank',
        'current_rank',
        'estimated_rank',
    ];

    public function results()
    {
        return $this->hasMany(RaceResult::class);
    }

    public function getUseEstimateAttribute(): bool
    {
        return config('app.use_estimates') && $this->results->count() < config('app.minimum_races');
    }

    public function getScoreAttribute(): float
    {
        return $this->use_estimate ? $this->estimated_score : $this->current_score;
    }

    public function getEstimatedScoreAttribute(): float
    {
        return Cache::remember('entrants:' . $this->id . ':estimated_score', 86400, function () {
            return $this->results->pluck('score')
                ->sort()
                // Calculate the mean
                ->average();
        });
    }

    public function getCurrentScoreAttribute(): float
    {
        return Cache::remember('entrants:' . $this->id . ':current_score', 86400, function () {
            return $this->results->pluck('score')
                ->pad(config('app.minimum_races'), 0)
                ->sort()
                // Remove the top and bottom scores
                ->skip(config('app.drop_extremes_count') ?: intval(floor(max(0, $this->results->count() - config('app.minimum_races')) / 2)))
                ->reverse()
                ->skip(config('app.drop_extremes_count') ?: intval(floor(max(0, $this->results->count() - config('app.minimum_races')) / 2)))
                // Calculate the mean
                ->average();
        });
    }

    public function getRankAttribute(): int
    {
        return $this->use_estimate ? $this->estimated_rank : $this->current_rank;
    }

    public function getEstimatedRankAttribute(): int
    {
        return Cache::remember('entrants:' . $this->id . ':estimated_rank', 86400, function () {
            $tmp = [];
            Entrant::all()->mapWithKeys(function ($item) {
                return [$item['id'] => ['id' => $item['id'], 'score' => $item['score']]];
            })->sortByDesc('score')->values()->each(function ($item, $key) use (&$tmp) {
                $tmp[$item['id']] = $key;
            });

            return $tmp[$this->id] + 1;
        });
    }

    public function getCurrentRankAttribute(): int
    {
        return Cache::remember('entrants:' . $this->id . ':current_rank', 86400, function () {
            $tmp = [];
            Entrant::all()->mapWithKeys(function ($item) {
                return [$item['id'] => ['id' => $item['id'], 'score' => $item['current_score']]];
            })->sortByDesc('score')->values()->each(function ($item, $key) use (&$tmp) {
                $tmp[$item['id']] = $key;
            });

            return $tmp[$this->id] + 1;
        });
    }
}
