<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Race extends Model
{
    use HasFactory;

    public $timestamps = false;
    public $guarded = [];
    protected $casts = [
        'opened_at' => 'datetime',
        'started_at' => 'datetime',
        'ended_at' => 'datetime',
    ];

    public function results()
    {
        return $this->hasMany(RaceResult::class);
    }

    public function recalculatePar()
    {
        if ($this->results()->count() === 0) {
            $this->par_seconds = null;
            $this->save();
            return $this;
        }
        $par = intval(
            round(
                $this->results()
                    ->orderBy('finish_seconds')
                    ->limit(config('app.par_finishers'))
                    ->get('finish_seconds')
                    ->average('finish_seconds')
            )
        );
        $this->par_seconds = $par;
        $this->save();

        return $this;
    }
}
