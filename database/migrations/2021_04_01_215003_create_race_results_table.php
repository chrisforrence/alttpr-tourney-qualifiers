<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('race_results', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('race_id');
            $table->foreign('race_id')->references('id')->on('races')->onDelete('cascade');
            $table->unsignedBigInteger('entrant_id');
            $table->foreign('entrant_id')->references('id')->on('entrants')->onDelete('cascade');
            $table->unsignedInteger('finish_seconds')->nullable();
            $table->boolean('is_finished');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('race_results');
    }
};
